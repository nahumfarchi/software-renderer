# Software-Renderer

![Pipe Teapot](/img/teapot2.jpg)
![Teapot](/img/teapot1.jpg)
![Cow](/img/cow.jpg)

A mesh renderer implemented without using the GPU. This means that everything was built from the ground up using only a putPixel(x,y) type of function. It is able to render a 3D scene with multiple light sources in orthographic or perspective mode. Some of the features included are:

- Orthographic and perspective projection matrices
- Wireframe or solid rendering
- Model shading (Flat, Gouraud, or Phong)
- Shadow maps
- Z-buffer
- Backface culling
- Textures
- Anti-aliasing
- GUI for setting up the scene
- Saving the scene to an image

This project was done at the Technion while doing an introductory computer graphics course:
http://webcourse.cs.technion.ac.il/234325/Winter2015-2016/

It took second place: http://www.cs.technion.ac.il/%7Ecs234325/Hall-Of-Fame/Winter2015/Students_Work.htm
