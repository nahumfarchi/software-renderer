#include <algorithm>
#include "Face.h"
#include "myHelpers.h"
#include "Vector4d.h"
#include "Vertex.h"
#include "Matrix4d.h"
#include "PointTracker.h"
#include "ZBuffer.h"
#include "Light.h"
#include "Camera.h"


Face::Face()
{
}

Face::~Face()
{
}

void Face::addVertex(Vertex * v) {
	if (v) {
		vertices.push_back(v);
	}
	else {
		log_warning("v is NULL\n");
	}
}

void Face::addEdge(Edge * e) {
	if (e) {
		edges.push_back(e);
	}
	else {
		log_warning("e is NULL\n");
	}
}

void Face::calcPlane() {
	if (vertices.size() < 3) {
		log_error("Cannot calculate plane, number of vertices is less than 3.");
		return;
	}
	Vector4d A = vertices[0]->getCoord();
	Vector4d B = vertices[1]->getCoord();
	Vector4d C = vertices[2]->getCoord();
	Vector4d norm;
	double D;
	calculatePlane(A, B, C, norm, D);
	setNormal(norm.getX(), norm.getY(), norm.getZ(), D);
}

void Face::setNormal(const Vector4d & n) {
	normal = n;
}

void Face::setNormal(double A, double B, double C, double _D) {
	normal = normalize(Vector4d(A,B,C));
	D = _D;

	normal_pt1 = Vector4d(0, 0, 0);
	for (std::vector<Vertex*>::const_iterator v = vertices.begin(); v != vertices.end(); ++v) {
		normal_pt1 = normal_pt1 + (*v)->getCoord();
	}
	normal_pt1 = normal_pt1 / vertices.size();
	normal_pt2 = normal_pt1 - normal;
}

void Face::calcNormalEndPts() {
	normal_pt1 = Vector4d(0, 0, 0);
	for (std::vector<Vertex*>::const_iterator v = vertices.begin(); v != vertices.end(); ++v) {
		normal_pt1 = normal_pt1 + (*v)->getCoord();
	}
	normal_pt1 = normal_pt1 / vertices.size();
	normal_pt2 = normal_pt1 + normal;
}

void Face::inverseNormal() {
	normal = -normal;
	normal_pt2 = normal_pt1 + normal;
}

void Face::drawNormal(CDC *pDC, COLORREF c) {
	draw(pDC, normal_pt1, normal_pt2, c);
}

void Face::homegenizeNormalPts() {
	normal_pt1.homegenize();
	normal_pt2.homegenize();
}

void Face::transformNormal(const Matrix4d & transMat) {
	normal_pt1 = transMat * normal_pt1;
	normal_pt2 = transMat * normal_pt2;
	normal = ::transformVector(normal, transMat);
}

double closestZ(const Face & f) {
	double cz = std::numeric_limits<double>::lowest();
	std::vector<Vertex*> VL = f.getVertices();
	for (std::vector<Vertex*>::const_iterator v = VL.begin(); v != VL.end(); ++v) {
		if ((*v)->getZ() > cz) {
			cz = (*v)->getZ();
		}
	}
	return cz;
}

void scanline(CDC *pDC, PointTracker & PT1, PointTracker & PT2, COLORREF c, 
	ZBuffer * zbuf /*=NULL*/, Vector4d * norm /*=NULL*/) 
{
	int y = PT1.y;
	int leftx, rightx;
	double leftz, rightz;
	double x;
	if (floor(PT1.x) < floor(PT2.x)) {
		x = PT1.x;
		leftx = floor(PT1.x);
		rightx = floor(PT2.x);
		leftz = PT1.z;
		rightz = PT2.z;
	}
	else {
		x = PT2.x;
		leftx = floor(PT2.x);
		rightx = floor(PT1.x);
		leftz = PT2.z;
		rightz = PT1.z;
	}
	double interpolatedz;
	double zhinc;
	if (leftx == rightx) {
		zhinc = 0;
	}
	else {
		zhinc = (rightz - leftz) / (rightx - leftx);
	}
	interpolatedz = leftz - zhinc;
	
	for (int ix = leftx; ix <= rightx; ++ix) {
		interpolatedz += zhinc;
		bool hasCloserZ = zbuf && (interpolatedz > zbuf->get(ix, y));
		if (hasCloserZ) {
			zbuf->set(ix, y, interpolatedz);
			double cz = -255*interpolatedz / 50;
			pDC->SetPixel(ix, y, c);
		}
		else if (!zbuf) {
			pDC->SetPixel(ix, y, c);
		}
	}
}

// calculate the triangle's area * 2
double triangleArea2(const Vector4d &a, const Vector4d &b, const Vector4d &c)
{
	return (c.getX() - a.getX()) * (b.getY() - a.getY()) - (c.getY() - a.getY()) * (b.getX() - a.getX());
}

void bycentric_coords(Vector4d v0, Vector4d v1, Vector4d v2, Vector4d p,
	double &w1, double &w2, double &w3) 
{
	// calculate the area of the triangle (*2) formed by v0, v1, v2 and the bycentric coordinates of the point p.
	double area = triangleArea2(v0, v1, v2);
	w1 = triangleArea2(v1, v2, p) / area;
	w2 = triangleArea2(v2, v0, p) / area;
	w3 = triangleArea2(v0, v1, p) / area;
}

void scanline_bycentric(CDC *pDC,
	const Vertex & v1, const Vertex & v2, const Vertex & v3,
	PointTracker & PT1, PointTracker & PT2, COLORREF c,
	ZBuffer * zbuf /*=NULL*/) 
{
	int leftx = PT1.x;
	int rightx = PT2.x;
	int y = PT1.y;
	double w1, w2, w3;

	for (int x = leftx; x <= rightx; x++) {
		Vector4d p(leftx, y, 0);
		bycentric_coords(v1.getCoord(), v2.getCoord(), v3.getCoord(), p, w1, w2, w3);
		double z = w1*v1.getZ() + w2*v2.getZ() + w3*v3.getZ();
		
		if (zbuf && z > zbuf->get(x, y)) {
			zbuf->set(x, y, z);
			pDC->SetPixel(x, y, RGB(w1*255,w2*255,w3*255));
		}
		else if (!zbuf) {
			pDC->SetPixel(x, y, c);
		}
	}
}

void fillTriangleScanline(
	CDC *pDC, 
	const Vertex & v1, const Vertex & v2, const Vertex & v3, COLORREF c,
	ZBuffer * zbuf /*=NULL*/, Vector4d * norm /*=NULL*/) 
{
	PointTracker PT_13(v1, v3);
	PointTracker PT_23(v2, v3);
	while (PT_13.isValid() && PT_23.isValid()) {
		scanline(pDC, PT_13, PT_23, c, zbuf, norm);
		PT_13.yStep();
		PT_23.yStep();
	}

	PointTracker PT_12(v1, v2);
	while (PT_12.isValid() && PT_13.isValid()) {
		scanline(pDC, PT_12, PT_13, c, zbuf, norm);
		PT_12.yStep();
		PT_13.yStep();
	}
}

void fillTriangleBycentric(
	CDC *pDC,
	const Vertex & v1, const Vertex & v2, const Vertex & v3, COLORREF c,
	ZBuffer * zbuf /*=NULL*/, Vector4d * norm /*=NULL*/)
{
	float area = triangleArea2(v1.getCoord(), v2.getCoord(), v3.getCoord());
	int xl = min(min(v1.getX(), v2.getX()), v3.getX());
	int xr = max(max(v1.getX(), v2.getX()), v3.getX());
	int yt = min(min(v1.getY(), v2.getY()), v3.getY());
	int yb = max(max(v1.getY(), v2.getY()), v3.getY());
	for (int x = xl; x <= xr; x++) {
		for (int y = yt; y <= yb; y++) {
			Vector4d p((double)x + 0.5, (double)y + 0.5, 0);
			double w1 = triangleArea2(v2.getCoord(), v3.getCoord(), p);
			double w2 = triangleArea2(v3.getCoord(), v1.getCoord(), p);
			double w3 = triangleArea2(v1.getCoord(), v2.getCoord(), p);
			if (w1 >= 0 && w2 >= 0 && w3 >= 0) {
				w1 /= area;
				w2 /= area;
				w3 /= area;
				double z = w1*v1.getZ() + w2*v2.getZ() + w3*v3.getZ();

				if (SHADING_MODEL == GOURAUD) {
					int r1 = w1*GetRValue(v1.c);
					int g1 = w1*GetGValue(v1.c);
					int b1 = w1*GetBValue(v1.c);
					int r2 = w2*GetRValue(v2.c);
					int g2 = w2*GetGValue(v2.c);
					int b2 = w2*GetBValue(v2.c);
					int r3 = w3*GetRValue(v3.c);
					int g3 = w3*GetGValue(v3.c);
					int b3 = w3*GetBValue(v3.c);

					c = RGB(r1 + r2 + r3, g1 + g2 + g3, b1 + b2 + b3);
				}
				else if (SHADING_MODEL == PHONG) {
					Vector4d interpolated_normal = w1*v1.getNormal() + w2*v2.getNormal()+ w3*v3.getNormal();
					interpolated_normal = normalize(interpolated_normal);
					Vector4d P = normalize(SCREEN_TO_CAMERA * Vector4d(x, y, z));
					c = pointShader(P, Vector4d(0, 0, 0), interpolated_normal, v1.kr, v1.kg, v2.kb);
				}

				if (zbuf && z > zbuf->get(x, y)) {
					zbuf->set(x, y, z);
					pDC->SetPixel(x, y, c);
				}
				else if (!zbuf) {
					pDC->SetPixel(x, y, c);
				}

			}
		}
	}
}

void Face::fill(CDC *pDC, COLORREF c, ZBuffer * zbuf /*=NULL*/) {
	auto v1 = vertices.begin();
	auto v2 = vertices.begin() + 1;
	auto v3 = vertices.begin() + 2;
	for (; v3 != vertices.end(); ++v2, ++v3) {
		std::vector<Vertex*> vertices;
		vertices.push_back(*v1); vertices.push_back(*v2); vertices.push_back(*v3);
		fillTriangleBycentric(pDC, *vertices[0], *vertices[1], *vertices[2], this->c, zbuf, &normal);
		fillTriangleBycentric(pDC, *vertices[2], *vertices[1], *vertices[0], this->c, zbuf, &normal);
	}
}

void log_debug_face(const Face & f) {
	std::vector<Vertex*> VL = f.getVertices();
	log_debug_less("[%d : \n", VL.size());
	log_debug_less("\tNormal "); log_debug_vertex(f.getNormal());
	//log_debug_less("\tnormal_pt1 "); log_debug_vertex(f.normal_pt1);
	//log_debug_less("\tnormal_pt2 "); log_debug_vertex(f.normal_pt2);
	log_debug_less("\tkr, kg, kb = %f, %f, %f\n", f.kr, f.kg, f.kb);
	log_debug_less("\tr, g, b = %d, %d, %d\n", GetRValue(f.c), GetGValue(f.c), GetBValue(f.c));
	for (int i = 0; i < VL.size(); ++i) {
		log_debug_less("\t");
		log_debug_vertex(*VL.at(i));
	}
	log_debug_less(" ]\n");
}

// Calculate the face color (flat shading model)
void Face::calcColor(const Vector4d & vp) {
	Vector4d P = normal_pt1;
	c = pointShader(
		vertices.back()->getCoord(),
		vp,
		normal,
		kr, kg, kb);
}