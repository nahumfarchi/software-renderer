#pragma once
#include "StdAfx.h"
#include <vector>

typedef enum 
{
    LIGHT_ID_AMBIENT=-1,
    LIGHT_ID_1=0,
    LIGHT_ID_2,
    LIGHT_ID_3,
    LIGHT_ID_4,
    LIGHT_ID_5,
    LIGHT_ID_6,
    LIGHT_ID_7,
    LIGHT_ID_8,
    MAX_LIGHT
} LightID;

typedef  enum 
{
    LIGHT_TYPE_DIRECTIONAL,
    LIGHT_TYPE_POINT,
    LIGHT_TYPE_SPOT
} LightType;

typedef  enum 
{
    LIGHT_SPACE_VIEW,
    LIGHT_SPACE_LOCAL
} LightSpace;

typedef enum
{
	FLAT,
	GOURAUD,
	PHONG
} ShadingModel;

class LightParams
{
public:

    //light enabled
    bool enabled;
    //type directional,point,spot
    LightType type;
    //local or view space
    LightSpace space;

    //color 0-255 RGB
    int colorR;
    int colorG;
    int colorB;

    //position
    double posX;
    double posY;
    double posZ;

    //direction
    double dirX;
    double dirY;
    double dirZ;
    
    LightParams():
		enabled(false),
		type(LIGHT_TYPE_DIRECTIONAL),
		space(LIGHT_SPACE_VIEW),
		colorR(255),colorG(255),colorB(255),
		posX(0),posY(0),posZ(0),
		dirX(0),dirY(0),dirZ(0)
    {}

protected:
private:
};

extern std::vector<LightParams> LIGHTS; // Data structure used to store all of the light sources.
extern LightParams AMBIENT;
extern ShadingModel SHADING_MODEL;
extern double SPECULAR_REFLECTION_EXP;

// Calculate the color on the given point.
class Vector4d;
COLORREF pointShader(
	const Vector4d &P,
	const Vector4d &cameraP,
	const Vector4d &normal,
	double kr, double kg, double kb);