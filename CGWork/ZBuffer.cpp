#include "ZBuffer.h"
#include <limits>
#include "myHelpers.h"


ZBuffer::ZBuffer(int w, int h)
{
	allocate(w, h);
}

ZBuffer::~ZBuffer()
{
	free();
}

void ZBuffer::resize(int w, int h) {
	free();
	allocate(w, h);
}

void ZBuffer::set(int x, int y, double z) {
	if (x > 0 && x < width && y > 0 && y < height) {
		buffer[y][x] = z;
	}
}

double ZBuffer::get(int x, int y) const {
	if (x > 0 && x < width && y > 0 && y < height) {
		return buffer[y][x];
	}
	return std::numeric_limits<double>::lowest();
}

void ZBuffer::free() {
	for (int r = 0; r < height; ++r) {
		delete[] buffer[r];
	}
	delete[] buffer;
}

void ZBuffer::allocate(int w, int h) {
	//TODO allocate as one contigious memory block of size h*w. The current way is not very efficient.
	buffer = new double*[h];
	for (int r = 0; r < h; ++r) {
		buffer[r] = new double[w];
		for (int c = 0; c < w; ++c) {
			buffer[r][c] = std::numeric_limits<double>::lowest(); // set to be as far away as possible.
		}
	}
	width = w;
	height = h;
}

void ZBuffer::reset() {
	for (int r = 0; r < height; ++r) {
		for (int c = 0; c < width; ++c) {
			buffer[r][c] = std::numeric_limits<double>::lowest(); // set to be as far away as possible.
		}
	}
}