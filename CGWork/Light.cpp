#include "Light.h"
#include "Vector4d.h"
#include "myHelpers.h"
#include "Vertex.h"

std::vector<LightParams> LIGHTS(MAX_LIGHT);
LightParams AMBIENT;
ShadingModel SHADING_MODEL;
double SPECULAR_REFLECTION_EXP = 20;

COLORREF pointShader(
	const Vector4d &P,
	const Vector4d &cameraP,
	const Vector4d &normal,
	double kr, double kg, double kb)
{
	double newr = 0, newg = 0, newb = 0;
	int light_srcs_count = 0;
	newr += kr * AMBIENT.colorR;
	newg += kg * AMBIENT.colorG;
	newb += kb * AMBIENT.colorB;
	light_srcs_count++;

	for (LightParams & light : LIGHTS) {
		if (light.enabled) {
			light_srcs_count++;

			Vector4d V = normalize(P - cameraP);
			Vector4d L;
			if (light.type == LIGHT_TYPE_POINT) {
				L = normalize(P - Vector4d(light.posX, light.posY, light.posZ));
			}
			else if (light.type == LIGHT_TYPE_DIRECTIONAL) {
				L = normalize(Vector4d(light.dirX, light.dirY, light.dirZ));
			}
			else if (light.type == LIGHT_TYPE_SPOT) {

			}

			// diffuse
			double diffuse_alpha = dot_product(normal, L);
			//if (diffuse_alpha > 0) {
				newr += light.colorR * kr * diffuse_alpha;
				newg += light.colorG * kg * diffuse_alpha;
				newb += light.colorB * kb * diffuse_alpha;
			//}

			// specular
			// Computer graphcis principles and practice 2nd ed, page 730
			double specular_alpha = dot_product(2 * dot_product(normal, L)*normal - L, V);
			//if (diffuse_alpha > 0) {
				specular_alpha = pow(specular_alpha, SPECULAR_REFLECTION_EXP);
				newr += light.colorR * kr * specular_alpha;
				newg += light.colorG * kg * specular_alpha;
				newb += light.colorB * kb * specular_alpha;
			//}
		}
	}

	newr = min(newr, 255);
	newg = min(newg, 255);
	newb = min(newb, 255);
	
	newr = max(newr, 0);
	newg = max(newg, 0);
	newb = max(newb, 0);

	return RGB(newr, newg, newb);
}